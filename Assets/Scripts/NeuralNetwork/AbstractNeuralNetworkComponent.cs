using UnityEngine;

public abstract class AbstractNeuralNetworkComponent : MonoBehaviour
{
	protected float output;

	public float GetOutputValue()
	{
		return output;
	}
}
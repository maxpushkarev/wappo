using UnityEngine;

public abstract class AbstractNeuralNetworkCellActivator : MonoBehaviour
{
	public abstract float ActivateNeuron(float s);
	public abstract float ActivateDerivative(float s);
}
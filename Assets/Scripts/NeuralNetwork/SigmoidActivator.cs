using UnityEngine;

public class SigmoidActivator : AbstractNeuralNetworkCellActivator
{
	public override float ActivateNeuron (float s)
	{
				return 1 / (1 + Mathf.Exp (-s));
	}

	//f'(s);
	public override float ActivateDerivative(float s)
	{
				return Mathf.Exp (-s) * Mathf.Pow ( (1 + Mathf.Exp (-s)), -2);
	}
}
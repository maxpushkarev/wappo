public class NeuralNetworkCellComponent : AbstractNeuralNetworkComponent
{
	public AbstractNeuralNetworkCellActivator activator;
	public AbstractNeuralNetworkComponent[] links;
	public float[] weights;

	public float sum;

	public virtual void Calculate()
	{
		int linksCount = links.Length;
		float s = 0;

		for (int i = 0; i<linksCount; i++) {

			AbstractNeuralNetworkComponent link = links [i];
			float w = weights [i];
			float signal = link.GetOutputValue ();

			s += signal * w;
		}

		sum = s;
		output = activator.ActivateNeuron(s);
	}
}
﻿using UnityEngine;

public class NeuralNetworkInputComponent : AbstractNeuralNetworkComponent
{
	public void PutInputValue(float val)
	{
		output = val;
	}
}
﻿using System;
using UnityEngine;

[Serializable]
public class NeuralNetworkTeacherTrend
{
	[SerializeField]
	private int maxBadTrendEpochCount = 100;
	[SerializeField]
	private int maxTrendSwitchCount = 1000;


	[SerializeField]
	private bool firstEpoch = true;
	[SerializeField]
	private float previousMaxError = 0;
	[SerializeField]
	private float badTrendEpochCount = 0;
	[SerializeField]
	private float previousDelta = -1;
	[SerializeField]
	private int trendSwitchCount = 0;

	public void ResetTrend()
	{
		firstEpoch = true;
		previousMaxError = 0;
		previousDelta = -1;

		ResetBadTrendData();
		ResetTrendSwitch();
	}

	public void ResetBadTrendData()
	{
		badTrendEpochCount = 0;	
	}

	public void ResetTrendSwitch()
	{
		trendSwitchCount = 0;
	}

	public bool AnalyzeError(float maxError)
	{
		if (firstEpoch)
		{
			previousMaxError = maxError;
			previousDelta = 0;
			firstEpoch = false;
			return true;
		}
		else
		{
			float deltaError = maxError - previousMaxError;
			previousMaxError = maxError;

			float prevSign = Mathf.Sign(previousDelta);
			float curSign = Mathf.Sign(deltaError);

			previousDelta = deltaError;

			if (prevSign * curSign < 0)
			{
				trendSwitchCount++;
				if (trendSwitchCount > maxTrendSwitchCount)
				{
					Debug.LogError("Unstable max error");
					return false;
				}
			}

			if (deltaError >= 0)
			{
				badTrendEpochCount++;
				if (badTrendEpochCount >= maxBadTrendEpochCount)
				{
					Debug.LogError("Bad trend of max error");
					return false;
				}
				return true;
			}
			else
			{
				badTrendEpochCount = 0;
				return true;
			}
		}
	}
}
﻿using UnityEngine;

[System.Serializable]
public class NeuralNetworkExampleArrayLayout
{
	[System.Serializable]
	public struct NeuralNetworkExampleRowData
	{
		private const float EPS = 0.001f;

		public float[] row;

		public bool EqualRow (NeuralNetworkExampleRowData obj, int outputLength)
		{
			float[] objRow = obj.row;

			int rowsLength = row.Length;
			int objRowsLength = objRow.Length;

			if (rowsLength != objRowsLength) {
				return false;
			}

			int rowsLengthMinusOutputLength = rowsLength - outputLength;

			for (int i = 0; i < rowsLengthMinusOutputLength; i++) {
				
				float rowMember = row [i];
				float objRowMember = objRow [i];

				if (Mathf.Abs(rowMember - objRowMember) > EPS) {
					return false;
				}
			}

			return true;
		}

	}

	public NeuralNetworkExampleRowData[] rows;

	public bool IsEqualRowInLayout(NeuralNetworkExampleArrayLayout.NeuralNetworkExampleRowData newRow, int outputLength)
	{
		int rowsLength = rows.Length;

		for (int i = 0; i < rowsLength; i++) {
		
			if(rows[i].EqualRow(newRow, outputLength))
			{
				return true;
			}
		
		}

		return false;
	}
}
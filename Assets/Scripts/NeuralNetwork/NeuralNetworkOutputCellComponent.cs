using UnityEngine;

public class NeuralNetworkOutputCellComponent : NeuralNetworkCellComponent
{
	public int outputIndex;
	[SerializeField]
	private NeuralNetworkRootComponent neuralNetworkRoot;

	public override void Calculate ()
	{
		base.Calculate ();
		neuralNetworkRoot.outputValues [outputIndex] = output;
	}
}
﻿using UnityEngine;

public class NeuralNetworkTeacherComponent : MonoBehaviour
{
	public NeuralNetworkExampleArrayLayout examples;
	public float eta = 1;
	public float eps = 0.001f;
	public int iterateCall = 0;
	public int maxLessonIterationCalls = 300;
	public float minDefaultWeight = 0.15f;
	public float maxDefaultWeight = 0.25f;
	public int examplesCountForUsage = -1;
	public NeuralNetworkTeacherTrend trend;
}

using UnityEngine;

public class NeuralNetworkRootComponent : MonoBehaviour
{
	public NeuralNetworkInputComponent[] neuralNetworkInputs;
	public NeuralNetworkLayer[] neuralLayers;
	public float[] outputValues;

	private void Start()
	{
		gameObject.SetActive (false);
	}

	public void Calculate(params float[] inputVector)
	{
		int inputVectorLength = inputVector.Length;
		for (int i = 0; i<inputVectorLength; i++) {
			float inputVal = inputVector [i];
			neuralNetworkInputs [i].PutInputValue (inputVal);
		}

		int layersCount = neuralLayers.Length;
		for (int i = 0; i < layersCount; i++) {

			NeuralNetworkLayer layer = neuralLayers [i];
			NeuralNetworkCellComponent[] neurons = layer.neurons;
			int neuronsCount = neurons.Length;

			for (int j = 0; j < neuronsCount; j++) {
				NeuralNetworkCellComponent neuron = neurons [j];
				neuron.Calculate ();
			}

		}

	}
}
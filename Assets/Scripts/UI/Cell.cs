﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class Cell : MonoBehaviour {

	[SerializeField]
	private GameController game;
	[SerializeField]
	private RectTransform cellTransform;
	[SerializeField]
	private Image img;

	#if UNITY_EDITOR
		public Wall[] possibleWalls;
	#endif

	private Color defaultColor;
	private Color fadeColor;
	private Dictionary<CellCoord, Wall> walls;
	public Dictionary<CellCoord, Wall> Walls {
		get {
			return this.walls;
		}
	}

	private void Awake()
	{
		InitCell ();
	}

	public void InitCell()
	{
		walls = new Dictionary<CellCoord, Wall> ();
		defaultColor = img.color;
	}

	public RectTransform CellTransform {
		get {
			return this.cellTransform;
		}
	}

	public void SetFadeColor(Color fadeColor)
	{
		this.fadeColor = fadeColor;
	}

	public void InterpolateColor(float k){
		img.color = Color.Lerp (defaultColor, fadeColor, k);
	}

	public void AttachWall(CellCoord coord, Wall wall)
	{
		walls.Add (coord, wall);
		game.Walls.Add (wall);
	}
}

﻿using UnityEngine;
using UnityEngine.UI;

public class BtnClicker : MonoBehaviour {
	[SerializeField]
	private CellCoord delta;
	[SerializeField]
	private Button btnUI;
	[SerializeField]
	private BunnyController bunny;
	[SerializeField]
	private GameController gameController;


	public void MoveHandler()
	{
		if (gameController.InputLock) {
			return;
		}

		gameController.InputLock = true;
		bunny.MakeStep (delta);
	}

	public void Enable(bool val)
	{
		btnUI.interactable = val;
	}
}

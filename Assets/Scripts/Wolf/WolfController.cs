﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class WolfController : CharacterInCellController 
{
	private static readonly int WOLF_NEURAL_NETWORK_INPUT_LENGTH = 5;

	private const float RND_MIN = 0.0f;
	private const float RND_MAX = 1.0f;

	[SerializeField]
	private float minPossibilityForBestStep = 0.8f;
	[SerializeField]
	private float maxPossibilityForBestStep = 0.9f;

	[SerializeField]
	private float minRNDForDistance = 0.33f;

	[SerializeField]
	private float minUPRNDForDistance = 0.85f;
	[SerializeField]
	private float maxUPRNDForDistance = 0.95f;

	[SerializeField]
	private float minRNDDistance = 2;
	[SerializeField]
	private float maxRNDDistance = 4;

	[SerializeField]
	private BunnyController bunnyController;
	public NeuralNetworkRootComponent[] neuralNetworks;

	private NeuralNetworkRootComponent neuralNetwork;
	private List<CellCoord> possibleDirections;
	private float[] wolfNeuralNetworkInputs;
	private CellCoord[] gameCfgMovingVariants;
	private int gameCfgMovingVariantsLength;
	private int wallIndex;
	private float maxMinusMinRNDDistance;

	private float possibilityForBestStep;
	private float maxRNDForDistance;

	public void InitNeuralNetwork(int index, int wallsCount)
	{
		neuralNetwork = neuralNetworks[index];
		maxMinusMinRNDDistance = maxRNDDistance - minRNDDistance;
		GameConfiguration gameConfiguration = game.GameConfiguration;

		float minWallsCount = gameConfiguration.minWallsCount;
		float maxWallsCount = gameConfiguration.maxWallsCount;

		float wallsCoeff = Mathf.Clamp01(
			((float) (wallsCount - minWallsCount)) / 
			(maxWallsCount - minWallsCount)
		);

		possibilityForBestStep = Mathf.Lerp(minPossibilityForBestStep, maxPossibilityForBestStep, wallsCoeff);
		maxRNDForDistance = Mathf.Lerp(minUPRNDForDistance, maxUPRNDForDistance, wallsCoeff);
		
	}

	public override string CharacterName {
		get {
			return "Wolf";
		}
	}
		
	public override void InitCharacterInCellData ()
	{
		base.InitCharacterInCellData ();

		possibleDirections = new List<CellCoord> ();
		wolfNeuralNetworkInputs = new float[WOLF_NEURAL_NETWORK_INPUT_LENGTH];
		gameCfgMovingVariants = game.GameConfiguration.movingVariants;
		gameCfgMovingVariantsLength = gameCfgMovingVariants.Length;
	}

	public override GameResult CheckResultAfterStep ()
	{
		if (GetCurrentCell() == bunnyController.GetCurrentCell()) {
			return GameResult.LOST;
		}
		return GameResult.NONE;
	}	

	public override bool CanMakeStep (CellCoord delta)
	{
		bool val = base.CanMakeStep (delta);

		if (val) {
			possibleDirections.Add (delta);
		}

		return val;
	}

	public override bool ValidateNextStep ()
	{
		possibleDirections.Clear ();
		bool val = base.ValidateNextStep ();
		return val;
	}

	public override void PerformInput ()
	{
		if (bestStep.IsStepForWin)
		{
			MakeBestStep();
			return;
		}
			
		CellCoord bunnyCoord = bunnyController.CellCoord;

		float sqrDistanceFromBunny2Carrot = bunnyCoord.CalculateQuadDistance(carrotController.CellCoord);
		float distanceFromBunny2Carrot = Mathf.Sqrt (sqrDistanceFromBunny2Carrot);

		if (distanceFromBunny2Carrot <= minRNDDistance) {
			MakeBestStep();
			return;
		}

		float distance2Bunny = Mathf.Sqrt(cellCoord.CalculateQuadDistance(bunnyCoord));

		if (distanceFromBunny2Carrot <= distance2Bunny)
		{
			MakeBestStep();
			return;
		}

		float dist2BunnyCoeff = Mathf.Clamp01((distance2Bunny - minRNDDistance) / maxMinusMinRNDDistance);
		float possibilityForNN = Mathf.Lerp (minRNDForDistance, maxRNDForDistance, dist2BunnyCoeff);

		if (UnityEngine.Random.Range(RND_MIN, RND_MAX) < possibilityForNN)
		{
			MakeNNStep();
		}
		else
		{
			TryMakeBestStep ();
		}
	}

	private void MakeNNStep()
	{
		EncodeInputDataForWolfNeuralNetwork();
		neuralNetwork.Calculate(wolfNeuralNetworkInputs);
		float output = neuralNetwork.outputValues[0];

		if (float.IsNaN(output))
		{
			TryMakeBestStep();
			return;
		}

		if (float.IsInfinity(output))
		{
			TryMakeBestStep();
			return;
		}

		if (float.IsNegativeInfinity(output))
		{
			TryMakeBestStep();
			return;
		}

		if (float.IsPositiveInfinity(output))
		{
			TryMakeBestStep();
			return;
		}

		int movingVariantIndex = DecodeOutputDataForWolfNeuralNetwork(output, gameCfgMovingVariantsLength);
		CellCoord movingVariantFromNN = gameCfgMovingVariants[movingVariantIndex];
		CharacterStepInfo stepInfoNN = movingVariantsInfoMap[movingVariantFromNN];

		if (stepInfoNN.MaxStepLength == 0)
		{
			TryMakeBestStep();
			return;
		}

		MakeStep(movingVariantFromNN * stepInfoNN.BestStepLength);
	}

	private void MakeBestStep()
	{
		CharacterStepInfo bestStepInfo = bestStep.StepInfo;
		MakeStep(bestStep.Coord * bestStepInfo.BestStepLength);
	}

	private void TryMakeBestStep()
	{
		if (UnityEngine.Random.Range (RND_MIN, RND_MAX) < possibilityForBestStep) {
			MakeBestStep();
		} else {
			MakeRandomStep();
		}
	}


	private void MakeRandomStep()
	{
		possibleDirections.Remove(bestStep.Coord);
		CellCoord dir = possibleDirections[UnityEngine.Random.Range(0, possibleDirections.Count)];
		int length = movingVariantsInfoMap[dir].BestStepLength;

		MakeStep(dir * length);
	}

	private void EncodeInputDataForWolfNeuralNetwork()
	{
		int carrotCornerIndex = game.CarrotCornerIndex;

		CellCoord wolfCoord = cellCoord;
		CellCoord bunnyCoord = bunnyController.CellCoord;

		EncodeInputDataForWolfNeuralNetwork(carrotCornerIndex, wolfCoord, bunnyCoord);
	}

	private void EncodeInputDataForWolfNeuralNetwork(int carrotCornerIndex, CellCoord wolfCoord, CellCoord bunnyCoord)
	{
		wolfNeuralNetworkInputs [0] = Convert.ToSingle (carrotCornerIndex);

		wolfNeuralNetworkInputs [1] = Convert.ToSingle (wolfCoord.Top);
		wolfNeuralNetworkInputs [2] = Convert.ToSingle (wolfCoord.Left);

		wolfNeuralNetworkInputs [3] = Convert.ToSingle (bunnyCoord.Top);
		wolfNeuralNetworkInputs [4] = Convert.ToSingle (bunnyCoord.Left);
	}

	public int DecodeOutputDataForWolfNeuralNetwork(float output, int length)
	{
		float outputByMovingVariants = output * length;
		int movingVariantIndex = Mathf.FloorToInt(outputByMovingVariants);
		movingVariantIndex = Mathf.Clamp(movingVariantIndex, (int) 0, (int) (length - 1));
		return movingVariantIndex;
	}

	public override bool IsOptimalDistanceToRestrictionItem (int distance, CharacterStepInfo characterStepInfo)
	{
		if (distance < characterStepInfo.OptimalDistanceToRestrictionItem) {
			return true;
		}

		return false;
	}

}
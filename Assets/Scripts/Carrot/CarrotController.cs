﻿using UnityEngine;

public class CarrotController : ItemInCellController<ItemInCell> {

	[SerializeField]
	private float centerAmplitude = 1f;
	[SerializeField]
	private float amplitude = 0.25f;
	[SerializeField]
	private float speed = 1;

	private RectTransform cachedTransform;
	private float timer;

	private void Awake(){
		cachedTransform = item.ItemTransform;
		timer = 0;
	}

	private void Update(){
		float scale = centerAmplitude + amplitude * Mathf.Sin (speed * timer);	
		timer += Time.deltaTime;
		cachedTransform.localScale = new Vector3 (scale, scale, scale);
	}

	protected override void InitItemPosition (Cell cell){
		item.SetPosition (cell);
	}
}
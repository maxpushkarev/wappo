﻿using System;
using UnityEngine;

[Serializable]
public struct CellCoord
{
	private const int TOP_MULTIPLIER = 10;

	[SerializeField]
	private int top;
	[SerializeField]
	private int left;


	public int Top {
		get {
			return this.top;
		}
	}

	public int Left {
		get {
			return this.left;
		}
	}

	public CellCoord (int top, int left)
	{
		this.top = top;
		this.left = left;
	}
		
	private int RecalculateKey()
	{
		return this.top * TOP_MULTIPLIER + this.left;
	}

	public static CellCoord operator +(CellCoord one, CellCoord two)
	{
		return new CellCoord (one.top + two.top, one.left + two.left);
	}

	public static CellCoord operator *(CellCoord one, int i)
	{
		return new CellCoord (one.top * i, one.left * i);
	}
		
	public static CellCoord operator -(CellCoord one, CellCoord two)
	{
		return new CellCoord (one.top - two.top, one.left - two.left);
	}

	public static CellCoord operator -(CellCoord one)
	{
		return new CellCoord (-one.top, -one.left);
	}

	public static bool operator ==(CellCoord one, CellCoord two)
	{
		return one.Equals (two);
	}

	public static bool operator !=(CellCoord one, CellCoord two)
	{
		return !one.Equals (two);
	}

	public override bool Equals (object obj)
	{
		CellCoord cellCoordObj = (CellCoord)obj;
		return (this.GetHashCode () == cellCoordObj.GetHashCode ());
	}

	public override int GetHashCode ()
	{
		return RecalculateKey();
	}

	public CellCoord Opposite(int maxIndex)
	{
		CellCoord cellCoord = new CellCoord (
			maxIndex - this.top,
			maxIndex - this.left
		);
		return cellCoord;
	}

	public CellCoord Normalize()
	{
		int absTop = Mathf.Abs(top);
		int absLeft = Mathf.Abs(left);

		int _top = (absTop > 0) ? top / absTop : top;
		int _left = (absLeft > 0) ? left / absLeft : left;

		CellCoord cellCoord = new CellCoord (
			_top,
			_left
		);

		return cellCoord;
	}

	public int CalculateQuadDistance(CellCoord other)
	{
		CellCoord coordBetween = this - other;
		int top = coordBetween.Top;
		int left = coordBetween.Left;

		return top * top + left * left;
	}

	public override string ToString ()
	{
		return string.Format ("[CellCoord: Top={0}, Left={1}]", Top, Left);
	}
}
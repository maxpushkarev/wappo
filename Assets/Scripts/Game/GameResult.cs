﻿using System;

public enum GameResult
{
	NONE,
	WIN,
	LOST,
	BLOCK
}
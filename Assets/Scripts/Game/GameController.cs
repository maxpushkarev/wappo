﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class GameController : MonoBehaviour {

	private const string GAME_CFG_VARIANT_FORMAT = "Game config №{0}";

	[SerializeField]
	private Text logText;
	[SerializeField]
	private BunnyController bunnyController;
	[SerializeField]
	private WolfController wolfController;
	[SerializeField]
	private CarrotController carrotController;
	[SerializeField]
	private GameConfiguration gameConfiguration;
	[SerializeField]
	private ItemInCell carrot;
	[SerializeField]
	private CanvasGroup characterCanvasGroup;
	[SerializeField]
	private CanvasGroup btnCanvasGroup;
	[SerializeField]
	private CellArray[] cells;
	public CellArray[] Cells {
		get {
			return this.cells;
		}
	}

	[SerializeField]
	private float stepIntervalSec = 0.5f;
	[SerializeField]
	private float gridFadeSec = 0.25f;
	[SerializeField]
	private float startGameIntervalSec = 0.5f;
	[SerializeField]
	private int frameCountToStart = 16;
	[SerializeField]
	private int maxIndex = 5;
	public int MaxIndex {
		get {
			return this.maxIndex;
		}
	}
		
	private CharacterInCell activeCharacter;
	private CharacterInCell idleCharacter;
	private List<Cell> cellChain;
	private int stepLength;
	private float currentStepIntervalSec;
	private List<Wall> walls;
	public List<Wall> Walls {
		get {
			return this.walls;
		}
	}
	private GameResult gameResult;

	private float timer;
	private Transitions transition;
	private Transitions Transition{
		get{ return transition;}
		set {
			
			transition = value;
			enabled = true;

			switch (transition) {
				case Transitions.STEP:
					currentStepIntervalSec = stepIntervalSec * stepLength;
					timer = currentStepIntervalSec;
					break;
				case Transitions.START:
					ConfigureGame ();
					InterpolateColorWalls (0);
					timer = startGameIntervalSec;
					break;
				case Transitions.GRID_FADE_IN:
					timer = gridFadeSec;
					Color fadeColor = activeCharacter.FadeColor;
					SetFadeColorForCells (fadeColor);
					InterpolateColorCells (0);
					break;
				case Transitions.GRID_FADE_OUT:
					timer = gridFadeSec;
					InterpolateColorCells (1);
					break;
				default:
					break;
			};
		}
	}

	public GameConfiguration GameConfiguration {
		get {
			return this.gameConfiguration;
		}
	}

	private int carrotCornerIndex;
	public int CarrotCornerIndex {
		get {
			return this.carrotCornerIndex;
		}
	}

	private int wallsIndex;

	private bool inputLock;
	public bool InputLock {
		get {
			return this.inputLock;
		}
		set {
			SetBtnsLocked (!value);
			inputLock = value;
		}
	}

	private void Awake()
	{
		timer = 0;
		transition = Transitions.AWAKE;
		gameResult = GameResult.NONE;
		cellChain = new List<Cell>();
		InitWallsInGame ();
		inputLock = false;
	}
		
	public void InitWallsInGame()
	{
		walls = new List<Wall>();
	}

	private void Update(){

		if (transition == Transitions.AWAKE) {
			if (frameCountToStart <= 0) {
				StartGame ();
				return;
			}
			frameCountToStart--;
			return;
		}

		if (timer <= 0) {
			
			switch (transition) {
				case Transitions.START:
					characterCanvasGroup.alpha = 1;
					InterpolateColorWalls (1);
					logText.text = bunnyController.WaitingText;
					InputLock = false;
					enabled = false;
					break;
				case Transitions.GRID_FADE_IN:
					InterpolateColorCells (1);
					Transition = Transitions.STEP;
					break;
				case Transitions.STEP:
					activeCharacter.PositionInterpolator = 1;
					Transition = Transitions.GRID_FADE_OUT;
					break;
				case Transitions.GRID_FADE_OUT:
					InterpolateColorCells (0);
					enabled = false;
					if (gameResult == GameResult.NONE) {
						activeCharacter.ColorInterpolator = 0;
						idleCharacter.ColorInterpolator = 1;
						SwitchCharacters ();
					}
					break;				
				default:
					break;
			}
				
			return;
		}

		timer -= Time.deltaTime;

		switch (transition) {
			case Transitions.START:
				float startK = 1 - timer / startGameIntervalSec;
				characterCanvasGroup.alpha = startK;
				InterpolateColorWalls (startK);
				break;
			case Transitions.STEP:
				activeCharacter.PositionInterpolator = 1 - timer / currentStepIntervalSec ;
				break;
			case Transitions.GRID_FADE_IN:
				InterpolateColorCells(1 - timer / gridFadeSec);
				break;
			case Transitions.GRID_FADE_OUT:
				float k = timer / gridFadeSec;
				InterpolateColorCells (timer / gridFadeSec);
				if (gameResult == GameResult.NONE) {
					activeCharacter.ColorInterpolator = k;
					idleCharacter.ColorInterpolator = 1 - k;
				}
				break;
			default:
				break;
		}
	}

	private void SetFadeColorForCells(Color fadeColor)
	{
		int chainLength = cellChain.Count;
		for (int i = 0; i < chainLength; i++) {
			Cell cell = cellChain [i];
			cell.SetFadeColor (fadeColor);
		}
	}

	private void InterpolateColorCells(float k)
	{
		int chainLength = cellChain.Count;
		for (int i = 0; i < chainLength; i++) {
			Cell cell = cellChain [i];
			cell.InterpolateColor (k);
		}
	}

	private void InterpolateColorWalls(float k){
		int wallsLength = walls.Count;
		for (int i = 0; i < wallsLength; i++) {
			Wall wall = walls [i];
			wall.ColorInterpolator = k;
		}
	}

	private void SetBtnsLocked(bool isFree)
	{
		btnCanvasGroup.interactable = isFree;
	}

	private void ConfigureGame()
	{
		CellCoord carrotCorner = ChooseCarrotCorner ();
		PrepareItems (carrotCorner);
		int wallsCount = CreateWalls ();
		wolfController.InitNeuralNetwork(wallsIndex, wallsCount);
	}

	private int CreateWalls()
	{
		WallConfig[] possibleWalls = gameConfiguration.wallCfgs;
		int possibleWallsLength = possibleWalls.Length;
		wallsIndex = Random.Range (0, possibleWallsLength);

		WallConfig wallCfg = possibleWalls [wallsIndex];
		Wall[] walls = wallCfg.walls;
		int wallsLength = walls.Length;
		for (int i = 0; i < wallsLength; i++) {
			Wall wall = walls [i];
			wall.ColorInterpolator = 0;
			wall.gameObject.SetActive (true);
		}
		Debug.Log (string.Format (GAME_CFG_VARIANT_FORMAT, wallsIndex));
		return wallsLength;
	}

	private void PrepareItems(CellCoord cornerForCarrot)
	{
		CellCoord cornerForBunny = cornerForCarrot.Opposite (maxIndex);
		int topToBunny = (cornerForBunny.Top - cornerForCarrot.Top)/maxIndex;
		int leftToBunny = (cornerForBunny.Left - cornerForCarrot.Left)/maxIndex;
		CellCoord cellForWolf = cornerForCarrot + new CellCoord(topToBunny, leftToBunny);

		bunnyController.InitItem(cornerForBunny);
		carrotController.InitItem (cornerForCarrot);
		wolfController.InitItem (cellForWolf);

		bunnyController.ValidateNextStep ();
		activeCharacter = bunnyController.Item;
		idleCharacter = wolfController.Item;

		activeCharacter.ColorInterpolator = 1;
		idleCharacter.ColorInterpolator = 0;
	}

	private CellCoord ChooseCarrotCorner()
	{
		CellCoord[] possibleCorners = gameConfiguration.carrotCornerVariants;
		int possibleCornersLength = possibleCorners.Length;
		carrotCornerIndex = Random.Range (0, possibleCornersLength);
		return possibleCorners [carrotCornerIndex];
	}

	private void ManageGameResult()
	{
		if (gameResult == GameResult.NONE) {
			return;
		}

		btnCanvasGroup.gameObject.SetActive (false);

		if (gameResult == GameResult.WIN) {
			carrotController.gameObject.SetActive (false);
			logText.text = bunnyController.WinText;
			return;
		}

		if (gameResult == GameResult.BLOCK) {
			logText.text = idleCharacter.Controller.BlockText;
			return;
		}

		if (gameResult == GameResult.LOST) {
			bunnyController.gameObject.SetActive (false);
			logText.text = wolfController.WinText;
			return;
		}
	}

	private void SwitchCharacters()
	{
		if (gameResult == GameResult.NONE) {
			logText.text = idleCharacter.Controller.WaitingText;
		}

		CharacterInCell temp = activeCharacter;
		activeCharacter = idleCharacter;
		idleCharacter = temp;
		cellChain.Clear ();
		CharacterInCellController activeController = activeCharacter.Controller;
		activeController.PerformInput ();
	}

	public void MakeStep(Cell prevCell, Cell targetCell, CellCoord prevCellCoord, CellCoord targetCellCoord)
	{
		cellChain.Add (prevCell);
		cellChain.Add (targetCell);

		CellCoord coordWay = targetCellCoord - prevCellCoord;
		CellCoord coordWayNrm = coordWay.Normalize ();

		int innerHorizontalStep = coordWayNrm.Left;
		int innerVerticalStep = coordWayNrm.Top;

		int innerHorizontalCellIndex = prevCellCoord.Left + innerHorizontalStep;
		int innerVerticalCellIndex = prevCellCoord.Top + innerVerticalStep;

		while (innerHorizontalCellIndex != targetCellCoord.Left) {
			cellChain.Add (cells [innerVerticalCellIndex].cellRow [innerHorizontalCellIndex]);
			innerHorizontalCellIndex += innerHorizontalStep;
		}

		while (innerVerticalCellIndex != targetCellCoord.Top) {
			cellChain.Add (cells [innerVerticalCellIndex].cellRow [innerHorizontalCellIndex]);
			innerVerticalCellIndex += innerVerticalStep;
		}

		stepLength = cellChain.Count - 1;

		gameResult = activeCharacter.Controller.CheckResultAfterStep ();
		bool validNextStep = idleCharacter.Controller.ValidateNextStep ();
		if (gameResult == GameResult.NONE) {
			if (!validNextStep) {
				gameResult = GameResult.BLOCK;
			}
		}

		ManageGameResult ();
		Transition = Transitions.GRID_FADE_IN;
	}

	public void StartGame(){
		Transition = Transitions.START;
	}

	private enum Transitions
	{
		AWAKE,
		START,
		STEP,
		GRID_FADE_IN,
		GRID_FADE_OUT
	}

}

﻿using UnityEngine;

public class GameConfiguration : MonoBehaviour {
	public CellCoord[] movingVariants;
	public CellCoord[] carrotCornerVariants;
	public WallConfig[] wallCfgs;
	public GameController gameController;
	public int minWallsCount;
	public int maxWallsCount;
}
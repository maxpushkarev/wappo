﻿using UnityEngine;

public class BunnyController : CharacterInCellController {

	[SerializeField]
	private WolfController wolfController;
	[SerializeField]
	private BtnClicker[] btns;

	public override string CharacterName {
		get {
			return "Bunny";
		}
	}

	protected override bool CanMakeStep (int index)
	{
		bool outputVal = base.CanMakeStep (index);
		btns [index].Enable (outputVal);
		return outputVal;
	}

	public override GameResult CheckResultAfterStep ()
	{
		if (GetCurrentCell() == carrotController.GetCurrentCell()) {
			return GameResult.WIN;
		}
		return GameResult.NONE;
	}

	public override void PerformInput ()
	{
		game.InputLock = false;
	}

	public override bool IsOptimalDistanceToRestrictionItem (int distance, CharacterStepInfo characterStepInfo)
	{
		if (distance > characterStepInfo.OptimalDistanceToRestrictionItem) {
			return true;
		}

		return false;
	}
}

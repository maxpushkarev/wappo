﻿using System.Collections.Generic;
using UnityEngine;

public abstract class CharacterInCellController : ItemInCellController<CharacterInCell>
{
	private const string WAITING_TEXT_FORMAT = "Waiting for {0}...";
	private const string WIN_TEXT_FORMAT = "{0} wins...";
	private const string BLOCK_TEXT_FORMAT = "{0} wins... {1} is blocked...";

	[SerializeField]
	private CharacterInCellController enemy;
	[SerializeField]
	private int maxStepLength;
	[SerializeField]
	protected CellCoord[] movingVariants;
	[SerializeField]
	protected CarrotController carrotController;
	[SerializeField]
	protected ItemController restrictionItem;
	[SerializeField]
	protected ItemController targetItem;

	protected CharacterBestStep bestStep;
	protected Dictionary<CellCoord, CharacterStepInfo> movingVariantsInfoMap; 
	protected int movingVariantsLength;

	private void Awake()
	{
		InitCharacterInCellData ();
	}

	public virtual void InitCharacterInCellData()
	{
		movingVariantsLength = movingVariants.Length;
		movingVariantsInfoMap = new Dictionary<CellCoord, CharacterStepInfo>();

		for (int i = 0; i < movingVariantsLength; i++)
		{
			CellCoord movingVariant = movingVariants[i];
			movingVariantsInfoMap.Add(movingVariant, new CharacterStepInfo(this, targetItem, restrictionItem));
		}

		CellCoord firstMovingVariant = movingVariants [0];
		bestStep = new CharacterBestStep (movingVariantsInfoMap[firstMovingVariant], firstMovingVariant);
	}

	protected override void InitItemPosition (Cell cell)
	{
		item.SetPosition (cell, cell, 1);
	}

	public virtual void MakeStep(CellCoord delta)
	{
		CellCoord prevCellCoord = cellCoord;
		Cell prevCell = GetCurrentCell ();
		cellCoord += delta;
		Cell targetCell = GetCurrentCell ();
		item.SetPosition (prevCell, targetCell, 0);
		game.MakeStep(prevCell, targetCell, prevCellCoord, cellCoord);
	}

	public bool IsNextCoordPositionAcceptable(CellCoord possibleCellCoord)
	{
		bool isWithinBounds = IsNextStepWithinBounds (possibleCellCoord);

		if (!isWithinBounds) {
			return false;
		}

		bool restriction = CanStepIntoItem(possibleCellCoord, restrictionItem);
		if (!restriction) {
			return false;
		}

		return true;
	}

	public virtual bool CanMakeStep(CellCoord delta){

		CellCoord currentCellCoord = this.cellCoord;

		int maxPossibleStepLength = 0;
		CharacterStepInfo possibleStepInfo = movingVariantsInfoMap [delta];
		possibleStepInfo.ResetInfo ();

		for (int i = 0; i < maxStepLength; i++) {

			CellCoord possibleCellCoord = currentCellCoord + delta;

			bool isValidPossibleCoord = IsNextCoordPositionAcceptable(possibleCellCoord);
			if (!isValidPossibleCoord) {
				break;
			}

			bool isStoppedByWall = IsStoppedByWall (currentCellCoord, delta);
			if (isStoppedByWall) {
				break;
			}
				
			bool target = CanStepIntoItem(possibleCellCoord, targetItem);
			if (!target) {
				maxPossibleStepLength = i + 1;
				bestStep.SetBestStepAsWin();
				possibleStepInfo.CalculateStepProfit (delta, maxPossibleStepLength, bestStep);
				break;
			}

			currentCellCoord += delta;
			maxPossibleStepLength = i + 1;
			possibleStepInfo.CalculateStepProfit (delta, maxPossibleStepLength, bestStep);

		}

		possibleStepInfo.MaxStepLength = maxPossibleStepLength;
		return maxPossibleStepLength > 0;

	}

	private bool IsStoppedByWall(CellCoord cellCoord, CellCoord delta)
	{
		bool frontWall = GetCell (cellCoord).Walls.ContainsKey (delta);
		if (frontWall) {
			return true;
		}
		CellCoord targetCoord = cellCoord + delta;
		Cell targetCell = GetCell (targetCoord);
		bool backWall = targetCell.Walls.ContainsKey (-delta);
		if (backWall) {
			return true;
		}
		return false;
	}

	private bool IsNextStepWithinBounds(CellCoord possibleCellCoord)
	{
		int nextTop = possibleCellCoord.Top;
		int nextLeft = possibleCellCoord.Left;
		int maxIndex = game.MaxIndex;

		if (nextTop < 0) {
			return false;
		}

		if (nextTop > maxIndex) {;
			return false;
		}

		if (nextLeft < 0) {
			return false;
		}

		if (nextLeft > maxIndex) {
			return false;
		}

		return true;
	}

	private bool CanStepIntoItem(CellCoord possibleCellCoord, ItemController item)
	{
		CellCoord itemCellCoord = item.CellCoord;
		return !(itemCellCoord == possibleCellCoord);
	}


	protected virtual bool CanMakeStep(int index){
		CellCoord movingVariant = movingVariants [index];
		return CanMakeStep (movingVariant);
	}

	public virtual bool ValidateNextStep(){
		
		bool validateRes = false;
		bestStep.ResetBestStep();

		for (int i = 0; i < movingVariantsLength; i++) {
			validateRes |= CanMakeStep (i);
		}

		return validateRes;
	}

	public string WinText {
		get{ 
			return string.Format (WIN_TEXT_FORMAT, CharacterName);
		}
	}

	public string WaitingText {
		get{ 
			return string.Format (WAITING_TEXT_FORMAT, CharacterName);
		}
	}

	public string BlockText {
		get { 
			return string.Format (BLOCK_TEXT_FORMAT, enemy.CharacterName, CharacterName);
		}
	}

	public abstract string CharacterName {
		get;
	}

	public abstract GameResult CheckResultAfterStep ();
	public abstract bool IsOptimalDistanceToRestrictionItem (int distance, CharacterStepInfo characterStepInfo);
	public abstract void PerformInput ();
}
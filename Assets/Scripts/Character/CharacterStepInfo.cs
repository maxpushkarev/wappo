﻿public class CharacterStepInfo
{
	private const int DEFAULT_VALUE = 0;

	private int maxStepLength;
	public int MaxStepLength
	{
		get { return maxStepLength; }
		set { maxStepLength = value; }
	}

	private int bestStepLength;
	public int BestStepLength {
		get {
			return this.bestStepLength;
		}
	}

	private int optimalDistanceToTargetItem;
	public int OptimalDistanceToTargetItem {
		get {
			return this.optimalDistanceToTargetItem;
		}
	}

	private int optimalDistanceToRestrictionItem;
	public int OptimalDistanceToRestrictionItem {
		get {
			return this.optimalDistanceToRestrictionItem;
		}
	}

	private CharacterInCellController hostItem;
	private ItemController targetItem;
	private ItemController restrictionItem;
	private bool reset;
	public bool Reset {
		get {
			return this.reset;
		}
	}

	public CharacterStepInfo(CharacterInCellController hostItem, ItemController targetItem, ItemController restrictionItem)
	{
		this.hostItem = hostItem;
		this.targetItem = targetItem;
		this.restrictionItem = restrictionItem;

		ResetInfo();
	}

	public void ResetInfo()
	{
		if(reset)
		{
			return;
		}

		reset = true;
		maxStepLength = DEFAULT_VALUE;
		bestStepLength = DEFAULT_VALUE;
		optimalDistanceToRestrictionItem = DEFAULT_VALUE;
		optimalDistanceToTargetItem = DEFAULT_VALUE;
	}

	public void CalculateStepProfit(CellCoord delta, int length, CharacterBestStep bestStep)
	{
		CellCoord currentHostCoord = hostItem.CellCoord;
		CellCoord newHostCellCoord = currentHostCoord + delta*length;

		int distanceToTarget = CalculateQuadDistance (newHostCellCoord, targetItem);
		int distanceToRestriction = CalculateQuadDistance (newHostCellCoord, restrictionItem);

		if (reset) {
			reset = false;
			UpdateProfitValues (length, distanceToTarget, distanceToRestriction, bestStep, delta);
			return;
		}

		if (CompareProfitByDistances (distanceToTarget, distanceToRestriction, this)) {
			UpdateProfitValues (length, distanceToTarget, distanceToRestriction, bestStep, delta);
		}
	}

	private bool CompareProfitByDistances(int distanceToTarget, int distanceToRestriction, CharacterStepInfo characterStepInfo)
	{
		int optimalDistanceToTargetItem = characterStepInfo.OptimalDistanceToTargetItem;

		if (distanceToTarget < optimalDistanceToTargetItem) {
			return true;
		}

		if (distanceToTarget == optimalDistanceToTargetItem) {
			if (hostItem.IsOptimalDistanceToRestrictionItem (distanceToRestriction, characterStepInfo)) {
				return true;
			}
		}

		return false;
	}

	private void UpdateProfitValues(int length, int dist2Target, int dist2Restriction, CharacterBestStep bestStep, CellCoord delta)
	{
		this.bestStepLength = length;
		this.optimalDistanceToTargetItem = dist2Target;
		this.optimalDistanceToRestrictionItem = dist2Restriction;

		UpdateBestStepIfNeeded (bestStep, delta);
	}

	private void UpdateBestStepIfNeeded(CharacterBestStep bestStep, CellCoord delta)
	{
		CharacterStepInfo bestStepInfo = bestStep.StepInfo;

		if (bestStepInfo.Reset) {
			bestStep.SetBestStep (this, delta);
			return;
		}

		if (CompareProfitByDistances (
			this.OptimalDistanceToTargetItem, 
			this.OptimalDistanceToRestrictionItem, 
			bestStepInfo)
		) {
			bestStep.SetBestStep (this, delta);
		}
	}

	private int CalculateQuadDistance(CellCoord newHostCellCoord, ItemController item)
	{
		return newHostCellCoord.CalculateQuadDistance(item.CellCoord);
	}
	
}
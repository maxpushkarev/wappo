﻿public class CharacterBestStep
{
	private bool isStepForWin;
	public bool IsStepForWin
	{
		get { return isStepForWin; }
	}

	private CharacterStepInfo stepInfo;
	public CharacterStepInfo StepInfo {
		get {
			return this.stepInfo;
		}
	}

	private CellCoord coord;
	public CellCoord Coord {
		get {
			return this.coord;
		}
	}

	public void SetBestStepAsWin()
	{
		isStepForWin = true;
	}

	public CharacterBestStep (CharacterStepInfo stepInfo, CellCoord coord)
	{
		isStepForWin = false;
		SetBestStep (stepInfo, coord);
	}

	public void SetBestStep(CharacterStepInfo stepInfo, CellCoord coord)
	{
		this.stepInfo = stepInfo;
		this.coord = coord;
	}

	public void ResetBestStep()
	{
		isStepForWin = false;
		stepInfo.ResetInfo();
	}
}
﻿using UnityEngine;
using UnityEngine.UI;

public class CharacterInCell : ItemInCell
{
	[SerializeField]
	private Image characterImage;
	[Range(0.0f, 1.0f)]
	[SerializeField]
	private float minAlpha = 0.5f;
	[Range(0.0f, 1.0f)]
	[SerializeField]
	private float maxAlpha = 1.0f;

	[SerializeField]
	private CharacterInCellController controller;
	public CharacterInCellController Controller {
		get {
			return this.controller;
		}
	}

	[SerializeField]
	private Color fadeColor;
	public Color FadeColor {
		get {
			return this.fadeColor;
		}
	}

	private Cell initialCell;

	private Color baseColor;
	private float positionInterpolator;
	public float PositionInterpolator {

		set {
			positionInterpolator = value;
			UpdatePosition ();
		}
	}

	private float colorInterpolator;
	public float ColorInterpolator {
		set {
			colorInterpolator = value;
			Color newColor = baseColor;
			newColor.a = Mathf.Lerp (minAlpha, maxAlpha, colorInterpolator);
			characterImage.color = newColor;
		}
	}

	protected override void Awake ()
	{
		base.Awake ();
		baseColor = characterImage.color;
	}

	protected override void UpdatePosition ()
	{
		Vector3 initialPos = initialCell.CellTransform.position;
		Vector3 targetPos = targetCell.CellTransform.position;
		itemTransform.position = Vector3.Lerp(initialPos, targetPos, positionInterpolator);
	}

	public void SetPosition(Cell initialCell, Cell targetCell, float interpolator){
		this.initialCell = initialCell;
		this.positionInterpolator = interpolator;
		SetPosition (targetCell);
	}

}
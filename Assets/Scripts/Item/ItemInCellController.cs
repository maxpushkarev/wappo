﻿using UnityEngine;

public abstract class ItemInCellController<T> : ItemController where T : ItemInCell
{
	[SerializeField]
	protected T item;
	public T Item {
		get {
			return this.item;
		}
	}
}
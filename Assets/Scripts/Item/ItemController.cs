﻿using UnityEngine;

public abstract class ItemController : MonoBehaviour
{
	[SerializeField]
	protected GameController game;

	protected CellCoord cellCoord;
	public CellCoord CellCoord {
		get {
			return this.cellCoord;
		}
	}

	private Cell PrepareInitialCell(CellCoord cellCoord)
	{
		this.cellCoord = cellCoord;
		return GetCurrentCell ();
	}

	public Cell GetCurrentCell()
	{
		return GetCell (cellCoord);
	}

	public Cell GetCell(CellCoord cellCoord)
	{
		return game.Cells [cellCoord.Top].cellRow [cellCoord.Left];
	}

	public virtual void InitItem(CellCoord cellCoord)
	{
		Cell cell = PrepareInitialCell (cellCoord);
		InitItemPosition (cell);
	}

	protected abstract void InitItemPosition(Cell cell);

}

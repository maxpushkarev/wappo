﻿using UnityEngine;

public class ItemInCell : MonoBehaviour
{
	[SerializeField]
	protected RectTransform itemTransform;
	protected Cell targetCell;

	public RectTransform ItemTransform {
		get {
			return this.itemTransform;
		}
	}

	private void OnRectTransformDimensionsChange(){
		if (CanUpdatePosition ()) {
			UpdatePosition();
		}
	}

	private bool CanUpdatePosition(){
		return (targetCell != null);
	}

	protected virtual void Awake()
	{
		enabled = false;
	}

	protected virtual void UpdatePosition()
	{
		Vector3 targetPos = targetCell.CellTransform.position;
		itemTransform.position = targetPos;
	}

	public void SetPosition(Cell targetCell){
		this.targetCell = targetCell;
		UpdatePosition ();
	}
}
﻿using System;
using UnityEngine;

[Serializable]
public class WallConfig {
	public Wall[] walls;
}
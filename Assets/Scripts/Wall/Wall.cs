﻿using UnityEngine;
using UnityEngine.UI;

public class Wall : MonoBehaviour {
	[SerializeField]
	private Image img;
	[SerializeField]
	private CellCoord wallMoving;
	[SerializeField]
	private Cell cell;

	private float colorInterpolator;
	public float ColorInterpolator {
		set {
			colorInterpolator = value;
			Color newColor = img.color;
			newColor.a = colorInterpolator;
			img.color = newColor;
		}
	}

	private void Awake()
	{
		InitWall ();
	}

	public void InitWall()
	{
		cell.AttachWall(wallMoving, this);
	}
}
﻿using UnityEditor;
using UnityEngine;
using System.Linq;

public class NeuralNetworkLayerCunstructor : BaseWappoEditor {

	private const string NEURON_SUFFIX = "Neuron";
	private const string MENU_ITEM = "Wappo/NN Layer Constructor";
	private const string TITLE = "NN Layer Constructor";

	private const string NN_LABEL = "Neural Network";
	private const string NEURONS_COUNT_LABEL = "Neurons Count";

	[SerializeField]
	private NeuralNetworkLayer neuralNetworkLayer;
	[SerializeField]
	private int neuronsCount;

	protected override Vector2 WindowSize {
		get {
			return new Vector2(300, 200);
		}
	}

	protected override void InitializeCustomWindow ()
	{
		neuralNetworkLayer = FindObjectOfType<NeuralNetworkLayer> ();
	}

	protected override void DrawFields ()
	{
		DrawLabel (NN_LABEL);
		DrawObjectField<NeuralNetworkLayer> (ref neuralNetworkLayer);


		DrawLabel (NEURONS_COUNT_LABEL);
		neuronsCount = EditorGUILayout.IntField (neuronsCount);
	}

	protected override void DrawBtns ()
	{
		if (neuralNetworkLayer == null) {
			return;
		}

		if (neuronsCount <= 0) {
			return;
		}

		DrawIndexedBtn (0, APPLY_TITLE, Apply);

	}

	private void Apply()
	{
		NeuralNetworkRootComponent neuralNetworkRoot = neuralNetworkLayer.GetComponentInParent<NeuralNetworkRootComponent> ();
		NeuralNetworkInputComponent[] inputs = neuralNetworkRoot.neuralNetworkInputs;
		NeuralNetworkLayer[] layers = neuralNetworkRoot.neuralLayers;
		int layerIndex = layers.ToList ().IndexOf(neuralNetworkLayer);
		AbstractNeuralNetworkComponent[] previousLayer = (layerIndex == 0) 
			? inputs 
			: (AbstractNeuralNetworkComponent[])layers[layerIndex -  1].neurons;

		foreach (Transform child in neuralNetworkLayer.transform) {
			DestroyImmediate (child.gameObject);
		}

		NeuralNetworkCellComponent[] neurons = neuralNetworkLayer.neurons;
		int currentNeuronsCount = neurons.Length;

		for (int i = 0; i < currentNeuronsCount; i++) {
			DestroyImmediate (neurons [i]);
		}

		neurons = neuralNetworkLayer.neurons = new NeuralNetworkCellComponent[neuronsCount];
		string layerName = neuralNetworkLayer.gameObject.name;
		AbstractNeuralNetworkCellActivator activator = neuralNetworkRoot.GetComponentInChildren<AbstractNeuralNetworkCellActivator> ();

		for (int i = 0; i < neuronsCount; i++) {
			
			NeuralNetworkCellComponent newNeuron = (new GameObject ()).AddComponent<NeuralNetworkCellComponent> ();
			GameObject newNeuronGameObject = newNeuron.gameObject;
			newNeuronGameObject.name = layerName + NEURON_SUFFIX + (i + 1);

			RectTransform rect = newNeuronGameObject.AddComponent<RectTransform> ();
			rect.SetParent (neuralNetworkLayer.transform);

			rect.localScale = Vector3.one;
			rect.localPosition = Vector3.zero;
			rect.localRotation = Quaternion.identity;

			newNeuron.activator = activator;
			FillLinksBetweenLayers (newNeuron, previousLayer);

			neurons [i] = newNeuron;
		}

		if (layerIndex == layers.Length - 1) {
			return;
		}

		NeuralNetworkCellComponent[] nextNeurons = layers [layerIndex + 1].neurons;
		int nextNeuronsLength = nextNeurons.Length;

		for (int i = 0; i < nextNeuronsLength; i++) {
			NeuralNetworkCellComponent nextNeuron = nextNeurons [i];
			FillLinksBetweenLayers (nextNeuron, neurons);
		}
	}

	private void FillLinksBetweenLayers(NeuralNetworkCellComponent neuron, AbstractNeuralNetworkComponent[] previousElements)
	{
		int previousLayerCount = previousElements.Length;
		neuron.weights = new float[previousLayerCount];
		neuron.links = new AbstractNeuralNetworkComponent[previousLayerCount];

		for (int j = 0; j < previousLayerCount; j++) {
			neuron.links [j] = previousElements [j];
			neuron.weights [j] = 0.0f;
		}
	}

	protected override string Title {
		get {
			return TITLE;
		}
	}

	[MenuItem(MENU_ITEM)]
	public static void ShowWindow()
	{
		ShowBaseWindow<NeuralNetworkLayerCunstructor> ();
	}

}
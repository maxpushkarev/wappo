﻿using UnityEngine;
using UnityEditor;
using System.Linq;
using System;

public class NeuralNetworkSplitter : BaseWappoEditor
{
	[SerializeField]
	private NeuralNetworkTeacherComponent baseTeacher;
	[SerializeField]
	private NeuralNetworkRootComponent nnElementPrefab;
	[SerializeField]
	private WolfController wolf;
	[SerializeField]
	private Transform rootNN;

	protected override string Title
	{
		get { return "NN Splitter"; }
	}

	[MenuItem("Wappo/NN Splitter")]
	public static void ShowWindow()
	{
		ShowBaseWindow<NeuralNetworkSplitter> ();
	}

	protected override void InitializeCustomWindow()
	{
		wolf = FindObjectOfType<WolfController>();
	}

	protected override void DrawFields()
	{
		DrawLabel("Base teacher with examples");
		DrawObjectField(ref baseTeacher);
		DrawLabel("NN Element prefab");
		DrawObjectField(ref nnElementPrefab);
		DrawLabel("Wolf controller");
		DrawObjectField(ref wolf);
		DrawLabel("Root nn object");
		DrawObjectField(ref rootNN);
	}

	protected override void DrawBtns()
	{
		if (baseTeacher == null)
		{
			return;
		}

		if (nnElementPrefab == null)
		{
			return;
		}

		if (wolf == null)
		{
			return;
		}

		if (rootNN == null) {
			return;
		}

		DrawIndexedBtn(0, APPLY_TITLE, Apply);
	}

	private void Apply()
	{
		NeuralNetworkRootComponent[] nns = wolf.neuralNetworks;
		int nnCount = nns.Length;
		for (int i = 0; i < nnCount; i++) {

			string baseName = nnElementPrefab.name;
			NeuralNetworkRootComponent newNN = NeuralNetworkRootComponent.Instantiate (nnElementPrefab);
			newNN.name = baseName + i;

			Transform newNNTransform = newNN.transform;
			newNNTransform.SetParent (rootNN);
			newNNTransform.localScale = Vector3.one;
			newNNTransform.localPosition = Vector3.zero;
			newNNTransform.localRotation = Quaternion.identity;

			nns [i] = newNN;

			NeuralNetworkTeacherComponent teacher = newNN.GetComponent<NeuralNetworkTeacherComponent> ();

			NeuralNetworkExampleArrayLayout.NeuralNetworkExampleRowData[] newRows = 
				baseTeacher.examples.rows
					.ToList ()
					.Where (e => e.row[1] == i)
					.Select (e => {
						
						NeuralNetworkExampleArrayLayout.NeuralNetworkExampleRowData newData 
						= new NeuralNetworkExampleArrayLayout.NeuralNetworkExampleRowData();

						float[] baseRow = e.row;
						newData.row = baseRow.Take(1).Concat(baseRow.Skip(2)).ToArray();

						return newData;
					})
					.ToArray();

			teacher.examples.rows = newRows;
		}
	}
}

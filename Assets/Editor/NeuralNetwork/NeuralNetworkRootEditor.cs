﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;


[CustomEditor(typeof(NeuralNetworkRootComponent))]
public class NeuralNetworkRootEditor : Editor
{
	private const string TEST_BTN_CAPTURE = "T E S T";
	private const string TEACH_BTN_CAPTURE = "T E A C H";
	private const string RESUME_TEACH_BTN_CAPTURE = "RESUME TEACH";
	private const string CHECK_EXAMPLES_BTN_CAPTURE = "CHECK EXAMPLES";
	private const string DEFAULT_WEIGHTS_TITLE = "Set defaults weights...";
	private const string TEACH_TITLE = "Teaching by examples...";
	private const string TEST_TITLE = "Test by examples...";
	private const string CHECK_EXAMPLES_TITLE = "Check examples...";
	private const string ERROR_EXAMPLES_MSG_FORMAT = "Conflict between examples №{0} and №{1}";
	private const string SUCCESS_EXAMPLES_MSG = "No conflicts in examples";
	private const string PROGRESS_EXAMPLE_MSG = "Example № ";

	public override void OnInspectorGUI () {

		DrawDefaultInspector ();

		NeuralNetworkRootComponent root = (NeuralNetworkRootComponent)target;
		NeuralNetworkTeacherComponent teacher = root.GetComponent<NeuralNetworkTeacherComponent>();

		if (teacher == null)
		{
			return;
		}

		if (GUILayout.Button (TEACH_BTN_CAPTURE)) {
			TeachNeuralNetwork (root, teacher);
		}

		if (teacher.iterateCall > 0) {
			if (GUILayout.Button (RESUME_TEACH_BTN_CAPTURE)) {
				ResumeTeachNeuralNetwork (root, teacher);
			}
		}

		if (GUILayout.Button(TEST_BTN_CAPTURE))
		{
			TestNeuralNetwork(root, teacher);
		}

		if (GUILayout.Button(CHECK_EXAMPLES_BTN_CAPTURE))
		{
			CheckExamples(root, teacher);
		}
	}

	private void CheckExamples(NeuralNetworkRootComponent root, NeuralNetworkTeacherComponent teacher)
	{
		ClearConsole();

		NeuralNetworkExampleArrayLayout examplesLayout = teacher.examples;
		NeuralNetworkExampleArrayLayout.NeuralNetworkExampleRowData[] examples = examplesLayout.rows;
		
		int examplesCount = examples.Length;
		int outputSize = root.outputValues.Length;

		bool isConflict = false;
		float progress = 0;
		EditorUtility.DisplayProgressBar(CHECK_EXAMPLES_TITLE, string.Empty, progress);

		for (int i = 0; i < examplesCount; i++)
		{
			UpdateProgress(i, examplesCount, CHECK_EXAMPLES_TITLE);

			NeuralNetworkExampleArrayLayout.NeuralNetworkExampleRowData example1 = examples[i];

			for (int j = 0; j < examplesCount; j++)
			{
				NeuralNetworkExampleArrayLayout.NeuralNetworkExampleRowData example2 = examples[j];

				if (i == j)
				{
					continue;
				}

				if (example1.EqualRow(example2, outputSize))
				{
					Debug.LogError(string.Format(ERROR_EXAMPLES_MSG_FORMAT, i, i));
					isConflict = true;
					break;
				}
			}

			if (isConflict)
			{
				break;
			}
		}

		if (!isConflict)
		{
			Debug.Log(SUCCESS_EXAMPLES_MSG);	
		}

		EditorUtility.ClearProgressBar();
	}

	private void TestNeuralNetwork(NeuralNetworkRootComponent root, NeuralNetworkTeacherComponent teacher)
	{
		ClearConsole();

		NeuralNetworkExampleArrayLayout.NeuralNetworkExampleRowData[] examples = teacher.examples.rows;
		int examplesCount = examples.Length;
		int inputSize = root.GetComponentsInChildren<NeuralNetworkInputComponent>().Length;
		float[] actualOutputs = root.outputValues;

		float progress = 0;
		EditorUtility.DisplayProgressBar(TEST_TITLE, string.Empty, progress);

		for (int i = 0; i < examplesCount; i++)
		{
			float[] example = examples[i].row;
			float[] inputs = example.Take(inputSize).ToArray();
			float[] expectedOutputs = example.Skip(inputSize).ToArray();

			UpdateProgress(i, examplesCount, TEST_TITLE);
			root.Calculate(inputs);

			string log = "Test example... input : [" + String.Join(" ; ", inputs.Select(k => k.ToString()).ToArray()) + "] -- ";
			log += "Expected Output : [" + String.Join(" ; ", expectedOutputs.Select(k => k.ToString()).ToArray()) + "] -- ";
			log += "Actual Output : [" + String.Join(" ; ", actualOutputs.Select(k => k.ToString()).ToArray()) + "]";

			Debug.Log(log);
		}

		EditorUtility.ClearProgressBar();
	}

	private void UpdateProgress(int index, int examplesCount, string title)
	{
		int currentExampleIndex = index + 1;
		float progress = (((float)currentExampleIndex) / examplesCount);
		EditorUtility.DisplayProgressBar(title, PROGRESS_EXAMPLE_MSG + currentExampleIndex, progress);
	}

	private void TeachNeuralNetwork(NeuralNetworkRootComponent root, NeuralNetworkTeacherComponent teacher)
	{
		ClearConsole();

		SetDefaultWeights(root, teacher);
		WorkWithExamples(root, teacher, true);
	}

	private void ResumeTeachNeuralNetwork(NeuralNetworkRootComponent root, NeuralNetworkTeacherComponent teacher)
	{
		ClearConsole();
		WorkWithExamples(root, teacher, false);
	}

	private void SetDefaultWeights(NeuralNetworkRootComponent root, NeuralNetworkTeacherComponent teacher)
	{
		NeuralNetworkCellComponent[] neurons = root.GetComponentsInChildren<NeuralNetworkCellComponent>(true);
		float progress = 0;
		EditorUtility.DisplayProgressBar(DEFAULT_WEIGHTS_TITLE, string.Empty, progress);

		float minDefaultWeight = teacher.minDefaultWeight;
		float maxDefaultWeight = teacher.maxDefaultWeight;


		int neuronsCount = neurons.Length;
		for (int i = 0; i < neuronsCount; i++)
		{
			NeuralNetworkCellComponent neuron = neurons[i];

			progress = (((float)i + 1) / neuronsCount);
			EditorUtility.DisplayProgressBar(DEFAULT_WEIGHTS_TITLE, neuron.name, progress);

			float[] weights = neuron.weights;
			int weightsCount = weights.Length;

			for (int j = 0; j < weightsCount; j++)
			{
				weights[j] = Random.Range(minDefaultWeight, maxDefaultWeight);
			}
		}
	}

	private void WorkWithExamples(NeuralNetworkRootComponent root, NeuralNetworkTeacherComponent teacher, bool fromBegin)
	{
		NeuralNetworkTeacherTrend trend = teacher.trend;

		int inputSize = root.GetComponentsInChildren<NeuralNetworkInputComponent>().Length;
		int maxIterationCall = teacher.maxLessonIterationCalls;
		float lessonSpeedCoeff = teacher.eta;
		float errorEps = teacher.eps;

		float maxError = 0;

		if (fromBegin) {
			teacher.iterateCall = 0;
			trend.ResetTrend();
		}
		else
		{
			trend.ResetBadTrendData();
			trend.ResetTrendSwitch();
		}

		Dictionary<NeuralNetworkCellComponent, float> errorMap = 
			new Dictionary<NeuralNetworkCellComponent, float>();

		do
		{

			if (!IterateAllExamples(
				ref maxError,
				maxIterationCall,
				root,
				lessonSpeedCoeff,
				inputSize,
				teacher,
				errorMap
				))
			{
				return;
			}

			if (!trend.AnalyzeError(maxError))
			{
				EditorUtility.ClearProgressBar();
				return;
			}

		} while (
			(maxError > errorEps) && 
			(teacher.iterateCall < maxIterationCall)
			);

		if (teacher.iterateCall == maxIterationCall)
		{
			EditorUtility.ClearProgressBar();
			Debug.LogError("Too many iterations of training");
			return;
		}


		FinalizeLesson(teacher.iterateCall);

	}


	private bool IterateAllExamples(
		ref float maxError, 
		int maxIterationCall,
		NeuralNetworkRootComponent root,
		float lessonSpeedCoeff,
		int inputSize,
		NeuralNetworkTeacherComponent teacher,
		Dictionary<NeuralNetworkCellComponent, float> errorMap )
	{
		NeuralNetworkExampleArrayLayout.NeuralNetworkExampleRowData[] examples = teacher.examples.rows;
		int examplesCountForUsage = teacher.examplesCountForUsage;
		int examplesCount = (examplesCountForUsage < 0) ? examples.Length : examplesCountForUsage;
		maxError = 0;

		if (teacher.iterateCall % 1000 == 0)
		{
			float progress = ((float)teacher.iterateCall / maxIterationCall);
			if (EditorUtility.DisplayCancelableProgressBar(TEACH_TITLE + "...Epoch №" + teacher.iterateCall, string.Empty, progress))
			{
				EditorUtility.ClearProgressBar();
				Debug.LogError("Teacher canceled");
				return false;
			}
		}


		teacher.iterateCall++;

		for (int i = 0; i < examplesCount; i++)
		{
			float[] example = examples[i].row;
			float[] input = example.Take(inputSize).ToArray();
			float[] output = example.Skip(inputSize).ToArray();

			IterateExample(
				root,
				ref maxError,
				output,
				input,
				lessonSpeedCoeff,
				errorMap
			);
		}

		return true;
	}

	private void IterateExample(
		NeuralNetworkRootComponent root,
		ref float maxError,
		float[] expectedOutputs,
		float[] inputs,
		float lessonSpeedCoeff,
		Dictionary<NeuralNetworkCellComponent, float> errorMap)
	{

		errorMap.Clear();
		root.Calculate(inputs);

		NeuralNetworkLayer[] layers = root.neuralLayers;
		int layersCount = layers.Length;

		for (int i = layersCount - 1; i >= 0; i--)
		{
			NeuralNetworkLayer layer = layers[i];
			IterateExampleForLayer(
				layer,
				ref maxError,
				expectedOutputs,
				lessonSpeedCoeff,
				errorMap
			);	
		}
	}


	private void IterateExampleForLayer(
		NeuralNetworkLayer layer,
		ref float maxError,
		float[] expectedOutputs,
		float lessonSpeedCoeff,
		Dictionary<NeuralNetworkCellComponent, float> errorMap)
	{

		NeuralNetworkCellComponent[] neurons = layer.neurons;
		int neuronsCount = neurons.Length;

		for (int j = 0; j < neuronsCount; j++)
		{
			NeuralNetworkCellComponent neuron = neurons[j];
			CorrectNeuron(
				neuron, 
				expectedOutputs, 
				neuronsCount, 
				lessonSpeedCoeff, 
				ref maxError,
				errorMap
			);
		}
	}

	private void CorrectNeuron(
		NeuralNetworkCellComponent neuron,
		float[] expectedOutputs,
		int neuronsCount,
		float lessonSpeedCoeff,
		ref float maxError,
		Dictionary<NeuralNetworkCellComponent, float> errorMap
	)
	{
		float sum = neuron.sum;
		AbstractNeuralNetworkCellActivator neuronActivator = neuron.activator;
		float derivSum = neuronActivator.ActivateDerivative(sum);

		float neuronError = CalculateError(
			neuron,
			expectedOutputs,
			neuronsCount,
			ref maxError,
			errorMap
		);

		float sigma = neuronError * derivSum;
		float dw = lessonSpeedCoeff * sigma;

		AbstractNeuralNetworkComponent[] links = neuron.links;
		int linkLehgth = links.Length;
		float[] linkWeights = neuron.weights;

		for (int k = 0; k < linkLehgth; k++)
		{
			AbstractNeuralNetworkComponent link = links[k];
			float x = link.GetOutputValue();
			float dwx = dw * x;

			linkWeights[k] += dwx;

			SetNeuronErrorForPreviousLayers(
				sigma, 
				linkWeights[k], 
				link,
				errorMap
			);

		}
	}

	private void SetNeuronErrorForPreviousLayers(
		float sigma, 
		float w, 
		AbstractNeuralNetworkComponent link,
		Dictionary<NeuralNetworkCellComponent, float> errorMap
		)
	{
		if (link.GetType() != typeof (NeuralNetworkCellComponent))
		{
			return;
		}

		NeuralNetworkCellComponent neuronOnPreviousLayer = (NeuralNetworkCellComponent) link;
		float weightedBackError = sigma*w;

		if (errorMap.ContainsKey(neuronOnPreviousLayer))
		{
			errorMap[neuronOnPreviousLayer] += weightedBackError;
		}
		else
		{
			errorMap.Add(neuronOnPreviousLayer, weightedBackError);
		}

	}

	private float CalculateError(NeuralNetworkCellComponent neuron, 
		float[] expectedOutputs, 
		float neuronsCount, 
		ref float maxError,
		Dictionary<NeuralNetworkCellComponent, float> errorMap)
	{
		float neuronError = 0;

		if (neuron.GetType() == typeof (NeuralNetworkOutputCellComponent))
		{
			NeuralNetworkOutputCellComponent outputNeuron = (NeuralNetworkOutputCellComponent) neuron;
			float d = expectedOutputs[outputNeuron.outputIndex];
			float y = neuron.GetOutputValue();
			neuronError = (d - y);
			float neuronErrorAbs = Mathf.Abs (neuronError);

			if (neuronErrorAbs > maxError) 
			{
				maxError = neuronErrorAbs;
			}

		}
		else
		{
			neuronError = errorMap[neuron];
		}

		return neuronError;
	}

	private void FinalizeLesson(int iterationCalls)
	{
		EditorUtility.ClearProgressBar();
		Debug.Log("Neural Network is educated!!!!!!!!!!! Epoch number = "+iterationCalls);
	}

	private static void ClearConsole()
	{
		Debug.ClearDeveloperConsole();
	
		Type logEntries = Type.GetType("UnityEditorInternal.LogEntries, UnityEditor.dll");
		MethodInfo clearMethod = logEntries.GetMethod("Clear", BindingFlags.Static | BindingFlags.Public);
		clearMethod.Invoke(null, null);
	}
}
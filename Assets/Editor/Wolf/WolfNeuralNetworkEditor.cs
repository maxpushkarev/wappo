﻿using UnityEditor;
using UnityEngine;
using System.Linq;
using System;

public class WolfNeuralNetworkEditor : BaseWappoConfigurationEditor
{
	private const int WOLF_NEURAL_NETWORK_INPUT_LENGTH = 6;
	private const string MENU_ITEM = "Wappo/Wolf NN";
	private const string TITLE = "Wolf NN";

	private const int OUTPUT_SIGNAL_LENGTH = 1;
	private const int STEP_DIRECTIONS_COUNT = 4;

	private const float WIDTH = 300;
	private const float HEIGHT = 860;

	private const string WOLF_POSITION_FIELD = "Wolf position:";
	private const string WOLF_STEP_DIRECTION_FIELD = "Wolf step direction:";
	private const string BUNNY_POSITION_FIELD = "Bunny position:";
	private const string CARROT_CORNER_FIELD = "Carrot corner:";
	private const string CARROT_CONTROLLER_FIELD = "Carrot controller:";
	private const string BUNNY_CONTROLLER_FIELD = "Bunny Controller:";
	private const string WOLF_CONTROLLER_FIELD = "Wolf Controller:";
	private const string NEURAL_NETWORK_FIELD = "Wolf Neural Network:";

	private const string ADD_TITLE = "Add Example";
	private const string INVALID_COORD_FORMAT = "Invalid position for {0}";
	private const string INVALID_STEP = "Invalid possible step for wolf";

	private const string CELL_COORD_LEFT = "_Left:";
	private const string CELL_COORD_TOP = "_Top:";

	private const string INVALID_NEW_EXAMPLE_FOR_TEACHER = "Invalid new example for teacher";
	private const string SUCCESFULL_ADDITION_EXAMPLE = "Example for Wolf Neural Network added successfully";

	private static readonly string[] STEP_DIRECTION_NAMES = new string[]{"_left", "_up", "_right", "_down"};

	[SerializeField]
	private WolfController wolf;
	[SerializeField]
	private BunnyController bunny;
	[SerializeField]
	private CarrotController carrot;
	[SerializeField]
	private NeuralNetworkTeacherComponent neuralNetwork;
	[SerializeField]
	private CellCoord wolfCoord;
	[SerializeField]
	private CellCoord bunnyCoord;


	private CanvasGroup cnvGroup;
	private int cornerIndex;
	private int stepDirectionIndex;

	protected override Vector2 WindowSize {
		get {
			return new Vector2(WIDTH, HEIGHT);
		}
	}

	protected override string Title {
		get {
			return TITLE;
		}
	}

	[MenuItem(MENU_ITEM)]
	public static void ShowWindow()
	{
		ShowBaseWindow<WolfNeuralNetworkEditor> ();
	}


	protected override void DrawFields ()
	{
		DrawGameCFGField ();
		DrawLabel (CARROT_CONTROLLER_FIELD);
		DrawCarrotControllerField ();
		DrawLabel (WOLF_CONTROLLER_FIELD);
		DrawWolfControllerField ();
		DrawLabel (NEURAL_NETWORK_FIELD);
		DrawNeuralNetworkField ();
		DrawLabel (BUNNY_CONTROLLER_FIELD);
		DrawBunnyControllerField ();
		DrawCarrotCornerGroup ();
		DrawWallCfgIndexField();
	}

	private void DrawCarrotCornerGroup()
	{
		if (gameConfiguration == null) {
			return;
		}

		if (carrot == null) {
			return;
		}

		DrawLabel (CARROT_CORNER_FIELD);
		DrawSpace ();

		CellCoord[] carrotCornerVariants = gameConfiguration.carrotCornerVariants;
		string[] carrotCornerOptions = carrotCornerVariants.Select(s => " "+s.ToString()).ToArray();
		cornerIndex = GUILayout.SelectionGrid(cornerIndex, carrotCornerOptions, 1, EditorStyles.radioButton);

		CellCoord carrotCornerVariant = carrotCornerVariants[cornerIndex];
		carrot.InitItem(carrotCornerVariant);
	}


	private void DrawNeuralNetworkField()
	{
		DrawObjectField<NeuralNetworkTeacherComponent> (ref neuralNetwork);
	}

	private void DrawBunnyControllerField()
	{
		DrawObjectField<BunnyController> (ref bunny);
	}

	private void DrawWolfControllerField()
	{
		DrawObjectField<WolfController> (ref wolf);
	}

	private void DrawCarrotControllerField()
	{
		DrawObjectField<CarrotController> (ref carrot);
	}

	protected override void InitializeCustomConfigurationWindow ()
	{
		carrot = FindObjectOfType<CarrotController> ();
		bunny = FindObjectOfType<BunnyController> ();
		wolf = FindObjectOfType<WolfController> ();

		neuralNetwork = FindObjectOfType<NeuralNetworkTeacherComponent>();

		if (neuralNetwork == null) {
			return;
		}

		NeuralNetworkExampleArrayLayout.NeuralNetworkExampleRowData[] rows = neuralNetwork.examples.rows;
		int rowsLength = rows.Length;

		if(rowsLength == 0)
		{
			return;
		}

		if (wolf == null) {
			return;
		}

		NeuralNetworkExampleArrayLayout.NeuralNetworkExampleRowData lastRow = rows [rowsLength - 1];
		float[] floatRow = lastRow.row;

		cornerIndex = DecodeCarrotCornerIndex (floatRow);
		wallCfgIndex = DecodeWallsIndex (floatRow);

		wolfCoord = DecodeWolfPosition (floatRow);
		bunnyCoord = DecodeBunnyPosition (floatRow);

		stepDirectionIndex = wolf.DecodeOutputDataForWolfNeuralNetwork (
			floatRow [floatRow.Length - 1], 
			gameConfiguration.movingVariants.Length
		);
	}

	private void EncodeInputDataForWolfNeuralNetwork(float[] example, int carrotCornerIndex, int wallsIndex, CellCoord wolfCoord, CellCoord bunnyCoord)
	{
		example[0] = Convert.ToSingle(carrotCornerIndex);
		example[1] = Convert.ToSingle(wallsIndex);

		example[2] = Convert.ToSingle(wolfCoord.Top);
		example[3] = Convert.ToSingle(wolfCoord.Left);

		example[4] = Convert.ToSingle(bunnyCoord.Top);
		example[5] = Convert.ToSingle(bunnyCoord.Left);
	}

	private int DecodeCarrotCornerIndex(float[] array)
	{
		return Convert.ToInt32(array[0]);
	}

	private int DecodeWallsIndex(float[] array)
	{
		return Convert.ToInt32(array[1]);
	}


	private CellCoord DecodeWolfPosition(float[] array)
	{
		int top = Convert.ToInt32(array[2]);
		int left = Convert.ToInt32(array[3]);

		return new CellCoord(top, left);
	}


	private CellCoord DecodeBunnyPosition(float[] array)
	{
		int top = Convert.ToInt32(array[4]);
		int left = Convert.ToInt32(array[5]);

		return new CellCoord(top, left);
	}

	private void DrawApplyButton(int index)
	{
		DrawIndexedBtn(index, APPLY_TITLE, ApplyNNExample);
	}

	private void DrawResetButton(int index)
	{
		DrawIndexedBtn(index, RESET_TITLE, ResetNNExample);
	}

	private void ApplyNNExample()
	{
		ResetNNExample();

		cnvGroup.alpha = 1.0f;
		ApplyWalls ();
	}

	private void ResetNNExample()
	{
		cnvGroup.alpha = 0.0f;
		ResetWalls ();
	}

	protected override void DrawBtns ()
	{
		if (gameConfiguration == null) {
			DrawLabel (string.Format(NULL_FIELD_TEXT_FORMAT, GAME_CONFIGURATION_TEXT));
			return;
		}

		if (carrot == null) {
			DrawLabel (string.Format(NULL_FIELD_TEXT_FORMAT, CARROT_CONTROLLER_FIELD));
			return;
		}

		if (wolf == null) {
			DrawLabel (string.Format(NULL_FIELD_TEXT_FORMAT, WOLF_CONTROLLER_FIELD));
			return;
		}

		if (neuralNetwork == null) {
			DrawLabel (string.Format(NULL_FIELD_TEXT_FORMAT, NEURAL_NETWORK_FIELD));
			return;
		}

		if (bunny == null) {
			DrawLabel (string.Format(NULL_FIELD_TEXT_FORMAT, BUNNY_CONTROLLER_FIELD));
			return;
		}

		if (!IsWallCfgIndexValid())
		{
			int maxWallCfgIndex = gameConfiguration.wallCfgs.Length - 1;
			DrawLabel(string.Format(INVALID_WALL_INDEX_FORMAT, 0, maxWallCfgIndex));
			return;
		}
			
		cnvGroup = carrot.transform.parent.gameObject.GetComponent<CanvasGroup> ();

		if (!DrawWolfPositionField ()) {
			DrawBtnsWithoutAddExample ();
			return;
		}

		if (!DrawBunnyPositionField ()) {
			DrawBtnsWithoutAddExample ();
			return;
		}

		if (!DrawWolfStepDirectionField ()) {
			DrawBtnsWithoutAddExample ();
			return;
		}

		DrawBtnsWithAddExample ();

	}

	private void DrawAddExampleBtn(int index)
	{
		DrawIndexedBtn(index, ADD_TITLE, AddExampleForNN);
	}

	private void AddExampleForNN()
	{
		NeuralNetworkExampleArrayLayout examples = neuralNetwork.examples;
		NeuralNetworkExampleArrayLayout.NeuralNetworkExampleRowData[] rows = examples.rows;
		int rowsCount = rows.Length;
		int newRowsCount = rowsCount + 1;
		NeuralNetworkExampleArrayLayout.NeuralNetworkExampleRowData[] newRows = new NeuralNetworkExampleArrayLayout.NeuralNetworkExampleRowData[newRowsCount];

		Array.Copy (rows, newRows, rowsCount);

		int inputlength = WOLF_NEURAL_NETWORK_INPUT_LENGTH;

		float[] newExample = new float[inputlength+1];
		EncodeInputDataForWolfNeuralNetwork(newExample, cornerIndex, wallCfgIndex, wolfCoord, bunnyCoord);
		newExample [inputlength] = EncodeOutputDataForWolfNeuralNetwork (stepDirectionIndex, gameConfiguration.movingVariants.Length);

		NeuralNetworkExampleArrayLayout.NeuralNetworkExampleRowData newRow = new NeuralNetworkExampleArrayLayout.NeuralNetworkExampleRowData ();
		newRow.row = newExample;
		newRows [rowsCount] = newRow;

		if (examples.IsEqualRowInLayout (newRow, OUTPUT_SIGNAL_LENGTH)) {
			Debug.Log (INVALID_NEW_EXAMPLE_FOR_TEACHER);
			return;
		}

		examples.rows = newRows;
		EditorUtility.SetDirty (neuralNetwork.gameObject);
		Debug.Log (SUCCESFULL_ADDITION_EXAMPLE);
	}

	private float EncodeOutputDataForWolfNeuralNetwork(int movingVariantIndex, int length)
	{
		float floatMovingVariantIndex = Convert.ToSingle(movingVariantIndex);

		floatMovingVariantIndex += 0.5f;
		floatMovingVariantIndex /= length;

		return floatMovingVariantIndex;
	}

	private void DrawBtnsWithAddExample()
	{
		DrawResetButton(0);
		DrawApplyButton(1);
		DrawAddExampleBtn (2);
	}

	private void DrawBtnsWithoutAddExample()
	{
		DrawResetButton(0);
		DrawApplyButton(1);
	}

	private bool DrawWolfPositionField()
	{
		return DrawCharacterPositionField (WOLF_POSITION_FIELD, ref wolfCoord, wolf);
	}

	private bool DrawBunnyPositionField()
	{
		return DrawCharacterPositionField (BUNNY_POSITION_FIELD, ref bunnyCoord, bunny);
	}

	private bool DrawCharacterPositionField(string label, ref CellCoord position, CharacterInCellController character)
	{
		DrawLabel (label);
		DrawCellCoordField (ref position);

		bool res = character.IsNextCoordPositionAcceptable (position);

		if (!res) {
			DrawLabel (string.Format (INVALID_COORD_FORMAT, character.CharacterName));
			return res;
		}

		character.InitItem (position);
		return res;
	}

	private bool DrawWolfStepDirectionField()
	{	
		DrawLabel (WOLF_STEP_DIRECTION_FIELD);
		stepDirectionIndex = GUILayout.SelectionGrid(stepDirectionIndex, STEP_DIRECTION_NAMES, STEP_DIRECTIONS_COUNT, EditorStyles.radioButton);

		gameConfiguration.GetComponent<GameController> ().InitWallsInGame ();
		IterateCells (InitCell);
		InitWalls();
		wolf.InitCharacterInCellData ();
		CellCoord delta = gameConfiguration.movingVariants [stepDirectionIndex];
		bool res = wolf.CanMakeStep (delta);

		if (!res) {
			DrawLabel (INVALID_STEP);
		}

		return res;
	}

	private void InitCell(Cell cell)
	{
		cell.InitCell ();
	}

	private void InitWalls()
	{
		WallConfig cfg = gameConfiguration.wallCfgs [wallCfgIndex];
		Wall[] walls = cfg.walls;
		int wallsCount = walls.Length;

		for(int i=0; i< wallsCount; i++)
		{
			walls [i].InitWall ();
		}
	}

	private void DrawCellCoordField(ref CellCoord coord)
	{
		DrawLabel (CELL_COORD_TOP, false);
		int top = EditorGUILayout.IntField (coord.Top);

		DrawLabel (CELL_COORD_LEFT, false);
		int left = EditorGUILayout.IntField (coord.Left);

		coord = new CellCoord (top, left);
	}
}
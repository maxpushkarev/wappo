﻿using System;
using UnityEditor;
using UnityEngine;

public abstract class BaseWappoEditor : EditorWindow
{
	protected const string APPLY_TITLE = "Apply";
	protected const string RESET_TITLE = "Reset";

	private const float SPACE = 10;
	private const float BTN_WIDTH = 100;
	private const float BTN_HEIGHT = 25;

	private const float TOP = 200;
	private const float LEFT = 200;
	private const float WIDTH = 200;
	private const float HEIGHT = 300;

	private void OnGUI ()
	{
		DrawFields ();
		DrawBtns ();
	}

	protected static void ShowBaseWindow<T>() where T : BaseWappoEditor
	{
		T window = CreateInstance<T>();

		window.titleContent = new GUIContent(window.Title);
		Vector2 size = window.WindowSize;
		window.position = new Rect(window.WindowPosition, size);
		window.minSize = size;

		window.InitializeCustomWindow();
		window.Show();
	}

	protected virtual Vector2 WindowPosition
	{
		get
		{
			return new Vector2(LEFT, TOP);
		}
	}

	protected virtual Vector2 WindowSize
	{
		get
		{
			return new Vector2(WIDTH, HEIGHT);
		}
	}

	protected void DrawSpace()
	{
		GUILayout.Space(SPACE);
	}

	protected void DrawLabel(string text, bool space = true)
	{
		if (space)
		{
			DrawSpace();
		}
		GUILayout.Label(text);
	}

	protected void DrawBtn(int yOffset, string title, Action whatToDo)
	{
		Vector2 windowSize = position.size;
		Vector2 btnPos = new Vector2(
			SPACE,
			windowSize.y - SPACE - BTN_HEIGHT - yOffset
		);
		Vector2 btnSize = new Vector2(BTN_WIDTH, BTN_HEIGHT);
		if (GUI.Button(new Rect(btnPos, btnSize), title))
		{
			whatToDo();
		}
	}

	protected void DrawObjectField<T>(ref T component) where T : UnityEngine.Object
	{
		DrawSpace();
		component = (T)EditorGUILayout.ObjectField(
			component,
			typeof(T),
			true
		);
	}

	protected void DrawIndexedBtn(int verticalOffsetCount, string title, Action action)
	{
		DrawBtn(
			verticalOffsetCount * (int)(SPACE + BTN_HEIGHT),
			title,
			action
		);
	}

	protected abstract string Title { get; }
	protected abstract void InitializeCustomWindow();
	protected abstract void DrawFields();
	protected abstract void DrawBtns();
}

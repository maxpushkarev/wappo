﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GameConfiguration))]
public class GameConfigurationEditor : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		GameConfiguration gameConfiguration = (GameConfiguration) target;

		if (GUILayout.Button("Min and max walls cfgs"))
		{
			WallConfig[] wallCfgs = gameConfiguration.wallCfgs;
			int count = wallCfgs.Length;

			gameConfiguration.minWallsCount = int.MaxValue;
			gameConfiguration.maxWallsCount = int.MinValue;

			for (int i = 0; i < count; i++)
			{
				WallConfig wallConfig = wallCfgs[i];
				int wallsCount = wallConfig.walls.Length;

				if (wallsCount > gameConfiguration.maxWallsCount)
				{
					gameConfiguration.maxWallsCount = wallsCount;
				}

				if (wallsCount < gameConfiguration.minWallsCount)
				{
					gameConfiguration.minWallsCount = wallsCount;
				}
			}
		}
	}
}

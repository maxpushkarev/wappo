﻿using UnityEditor;
using UnityEngine;
using System;

public abstract class BaseWappoConfigurationEditor : BaseWappoEditor
{
	private const string WALL_INDEX_TEXT = "Wall config index:";

	protected const string INVALID_WALL_INDEX_FORMAT = "Wall index is out of range [{0}, {1}]";
	protected const string GAME_CONFIGURATION_TEXT = "Game configuration:";
	protected const string NULL_FIELD_TEXT_FORMAT = "{0} is null";

	[SerializeField]
	protected GameConfiguration gameConfiguration;
	[SerializeField]
	protected int wallCfgIndex;

	private Action<Wall> actionToDoWithWalls;

	protected override void InitializeCustomWindow()
	{
		gameConfiguration = FindObjectOfType<GameConfiguration>();
		InitializeCustomConfigurationWindow();
	}

	protected void DrawGameCFGField()
	{
		DrawLabel (GAME_CONFIGURATION_TEXT);
		DrawObjectField<GameConfiguration> (ref gameConfiguration);
	}


	protected void DrawWallCfgIndexField()
	{
		DrawLabel(WALL_INDEX_TEXT);
		DrawSpace();
		wallCfgIndex = EditorGUILayout.IntField(wallCfgIndex);
	} 

	protected bool IsWallCfgIndexValid()
	{
		int maxValCfgIndex = gameConfiguration.wallCfgs.Length - 1;

		if (wallCfgIndex < 0)
		{
			return false;
		}

		if (wallCfgIndex > maxValCfgIndex)
		{
			return false;
		}

		return true;
	}

	protected void ApplyWalls()
	{
		ResetWalls();
		WallConfig wallCfg = gameConfiguration.wallCfgs[wallCfgIndex];
		Wall[] walls = wallCfg.walls;
		int wallsCount = walls.Length;
		for (int i = 0; i < wallsCount; i++)
		{
			Wall wall = walls[i];
			wall.gameObject.SetActive(true);
		}
	}

	protected void ResetWalls()
	{
		IterateWalls(ResetWall);
	}

	private void ResetWall(Wall wall)
	{
		wall.gameObject.SetActive(false);
	}

	protected void IterateCells(Action<Cell> actionToDo)
	{
		GameController gameController = gameConfiguration.gameController;
		CellArray[] cells = gameController.Cells;
		int rowCount = cells.Length;
		for (int i = 0; i < rowCount; i++)
		{
			Cell[] cellArray = cells[i].cellRow;
			int cellsInRowCount = cellArray.Length;
			for (int j = 0; j < cellsInRowCount; j++)
			{
				Cell cell = cellArray[j];
				actionToDo (cell);
			}
		}
	}

	protected void IterateWalls(Action<Wall> actionToDo)
	{
		actionToDoWithWalls = actionToDo;
		IterateCells (IterateWalls);
	}

	private void IterateWalls(Cell cell)
	{
		Wall[] walls = cell.possibleWalls;
		int wallsCount = walls.Length;
		for (int k = 0; k < wallsCount; k++)
		{
			Wall wall = walls[k];
			actionToDoWithWalls(wall);
		}
	}

	protected abstract void InitializeCustomConfigurationWindow();
}
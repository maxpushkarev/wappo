﻿using UnityEditor;
using System.Collections.Generic;

public class WallConfigurationEditor : BaseWappoConfigurationEditor
{
	private const string MENU_ITEM = "Wappo/Wall Editor";

	private const string TITLE = "Wall Editor";
	private const string BAKE_TITLE = "Bake";

	private List<Wall> wallsToWrite;

	protected override string Title {
		get {
			return TITLE;
		}
	}

	protected override void InitializeCustomConfigurationWindow ()
	{
		wallsToWrite = new List<Wall> ();
		wallCfgIndex = gameConfiguration.wallCfgs.Length - 1;
	}

	[MenuItem(MENU_ITEM)]
	public static void ShowWindow()
	{
		ShowBaseWindow<WallConfigurationEditor> ();
	}

	protected override void DrawFields()
	{
		DrawGameCFGField ();
		DrawWallCfgIndexField ();
	}

	private void DrawBakeBtn()
	{
		DrawIndexedBtn(2, BAKE_TITLE, BakeWallsToCfg);
	}

	private void DrawApplyButton()
	{
		DrawIndexedBtn (1, APPLY_TITLE, ApplyWalls);
	}

	private void DrawResetBtn ()
	{
		DrawIndexedBtn (0, RESET_TITLE, ResetWalls);
	}

	protected override void DrawBtns ()
	{
		if (gameConfiguration == null) {
			DrawLabel (string.Format(NULL_FIELD_TEXT_FORMAT, GAME_CONFIGURATION_TEXT));
			return;
		}

		WallConfig[] wallCfgs = gameConfiguration.wallCfgs;
		int maxValCfgIndex = wallCfgs.Length - 1;

		if (!IsWallCfgIndexValid())
		{
			DrawLabel(string.Format(INVALID_WALL_INDEX_FORMAT, 0, maxValCfgIndex));
			return;
		}

		DrawBakeBtn ();
		DrawApplyButton ();
		DrawResetBtn ();
	}

	private void BakeWallsToCfg()
	{
		wallsToWrite.Clear ();

		IterateWalls (WriteWallIfActive);

		WallConfig[] wallCfgs = gameConfiguration.wallCfgs;
		WallConfig wallCfg = wallCfgs [wallCfgIndex];
		wallCfg.walls = wallsToWrite.ToArray ();
	}

	private void WriteWallIfActive(Wall wall)
	{
		if (wall.gameObject.activeSelf) {
			wallsToWrite.Add (wall);
		}
	}
}